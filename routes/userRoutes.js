const express = require('express');
const router = express.Router();

const userController = require('../controllers/userControllers');
const auth = require('../auth');

// routes
router.post('/checkEmail', userController.checkEmailExists);

router.post('/register', userController.registerUser);

router.post('/login', userController.loginUser);

router.post('/details', userController.getProfile);

module.exports = router;